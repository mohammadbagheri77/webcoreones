#pragma checksum "E:\TSHPANDA_PRO\WebCoreOne\WebCoreOne\Views\PAdmin\settings.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "809f8d2cd0a4151f4618335f95004e7685371804"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_PAdmin_settings), @"mvc.1.0.view", @"/Views/PAdmin/settings.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\TSHPANDA_PRO\WebCoreOne\WebCoreOne\Views\_ViewImports.cshtml"
using WebCoreOne;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\TSHPANDA_PRO\WebCoreOne\WebCoreOne\Views\_ViewImports.cshtml"
using WebCoreOne.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "E:\TSHPANDA_PRO\WebCoreOne\WebCoreOne\Views\_ViewImports.cshtml"
using WebCoreOne.ViewModels.Account;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "E:\TSHPANDA_PRO\WebCoreOne\WebCoreOne\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"809f8d2cd0a4151f4618335f95004e7685371804", @"/Views/PAdmin/settings.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"61c42b42a136e4b28a3e7bc060cc67f5aaa82f08", @"/Views/_ViewImports.cshtml")]
    public class Views_PAdmin_settings : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<SiteSetting>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "RoleValidatioGuid", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString(" btn btn-primary"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "E:\TSHPANDA_PRO\WebCoreOne\WebCoreOne\Views\PAdmin\settings.cshtml"
  
    ViewData["Title"] = "settings";
    Layout = "~/Views/Shared/Panel.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>settings</h1>\r\n\r\n<div class=\"btn-group mt-4 mb-4\" style=\"width: max-content;\">\r\n    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "809f8d2cd0a4151f4618335f95004e76853718044411", async() => {
                WriteLiteral("Role Validatio Guid");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</div>\r\n\r\n");
#nullable restore
#line 13 "E:\TSHPANDA_PRO\WebCoreOne\WebCoreOne\Views\PAdmin\settings.cshtml"
 if (Model.Count == 0)
{

}
else
{
    for (int i = 0; i < Model.Count; i++)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("<div class=\"card mt-2\">\r\n    <div class=\"card-header\">\r\n        Sitesting Key : <b>");
#nullable restore
#line 23 "E:\TSHPANDA_PRO\WebCoreOne\WebCoreOne\Views\PAdmin\settings.cshtml"
                      Write(Model[i].Key);

#line default
#line hidden
#nullable disable
            WriteLiteral("</b>\r\n\r\n    </div>\r\n    <div class=\"card-body\">\r\n        Sitesting Value : <b>");
#nullable restore
#line 27 "E:\TSHPANDA_PRO\WebCoreOne\WebCoreOne\Views\PAdmin\settings.cshtml"
                        Write(Model[i].Value);

#line default
#line hidden
#nullable disable
            WriteLiteral("</b>\r\n\r\n    </div>\r\n    <div class=\"card-footer\">\r\n        Last Time Value Changed : ");
#nullable restore
#line 31 "E:\TSHPANDA_PRO\WebCoreOne\WebCoreOne\Views\PAdmin\settings.cshtml"
                             Write(Model[i].LastTimeChanged);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n\r\n\r\n\r\n\r\n</div>\r\n");
#nullable restore
#line 38 "E:\TSHPANDA_PRO\WebCoreOne\WebCoreOne\Views\PAdmin\settings.cshtml"
    }
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<SiteSetting>> Html { get; private set; }
    }
}
#pragma warning restore 1591

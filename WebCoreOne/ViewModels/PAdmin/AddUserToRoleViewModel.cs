﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCoreOne.ViewModels.PAdmin
{
    public class AddUserToRoleViewModel
    {
        public AddUserToRoleViewModel()
        {
            UserRolres = new List<UserRoleViewModel>();
        }


        public string UserId { get; set; }

        public List<UserRoleViewModel> UserRolres { get; set; }
    }
    public class UserRoleViewModel
    {
        public string RoleName { get; set; }
        public bool IsSelected { get; set; }
    }

}



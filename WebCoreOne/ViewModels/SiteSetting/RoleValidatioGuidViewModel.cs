﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCoreOne.ViewModels.SiteSetting
{
    public class RoleValidatioGuidViewModel
    {
        public string Value { get; set; }
        public DateTime? LastTimeChanged { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCoreOne.ViewModels.Role
{
    public class IndexViewModell
    {
        public string RoleName { get; set; }
        public string RoleId { get; set; }
    }
}

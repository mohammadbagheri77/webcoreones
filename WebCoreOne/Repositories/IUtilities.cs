﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebCoreOne.ViewModels.Role;

namespace WebCoreOne.Repositories
{
    public interface IUtilities
    {
        public IList<ActionAndControllerName> AreaAndActionAndControllerNamesList();

        public IList<string> GetAllAreasNames();
        public string DataBaseRoleValidationGuid();


    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WebCoreOne.Models;

namespace WebCoreOne.Controllers
{
   // [Authorize(policy: "DynamicRole")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [Authorize(policy: "DynamicRole")]
        public IActionResult pageOne()
        {
            return View();
        }

        [Authorize(policy: "DynamicRole")]
        public IActionResult pageh1()
        {
            return View();
        }
        [Authorize(policy: "DynamicRole")]
        public IActionResult pageh2()
        {
            return View();
        }
        [Authorize(policy: "DynamicRole")]
        public IActionResult pageh3()
        {
            return View();
        }
        [Authorize(policy: "DynamicRole")]
        public IActionResult pageh4()
        {
            return View();
        }
        [Authorize(policy: "DynamicRole")]
        public IActionResult pageh5()
        {
            return View();
        }

         [Authorize(policy: "DynamicRole")]
        public IActionResult pageh6()
        {
            return View();
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

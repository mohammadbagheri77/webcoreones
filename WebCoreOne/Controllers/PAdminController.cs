﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using WebCoreOne.Models;
using WebCoreOne.Models.Context;
using WebCoreOne.Repositories;
using WebCoreOne.ViewModels.PAdmin;
using WebCoreOne.ViewModels.Role;
using WebCoreOne.ViewModels.SiteSetting;

namespace WebCoreOne.Controllers
{
    [Authorize(policy: "DynamicRole")]
   // [Authorize(Roles = "ادمین")]
    public class PAdminController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly AppDbContext _dbContex;
        private readonly IMemoryCache _memoryCache;
        private readonly IUtilities _utilities;
      

        public PAdminController(RoleManager<IdentityRole> roleManager,
                                UserManager<IdentityUser> userManager,
                                AppDbContext dbContext,
                                IMemoryCache memoryCache,
                                IUtilities utilities)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _dbContex = dbContext;
            _memoryCache = memoryCache;
            _utilities = utilities;

        }




        ////////////////////////////////{  Index  }//////////////////////////////////


        public IActionResult Index()
        {
            return View();
        }

        ////////////////////////////////{  Role  }//////////////////////////////////


        public IActionResult Roles()
        {
            var roles = _roleManager.Roles.ToList();
            return View(roles);
        }


        [HttpGet]
        public IActionResult AddRole()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddRole(string name)
        {
            if (string.IsNullOrEmpty(name)) return NotFound();
            var role = new IdentityRole(name);
            var result = await _roleManager.CreateAsync(role);
            if (result.Succeeded) return RedirectToAction("Roles");

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(role);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeletRole(string id)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();
            var role = await _roleManager.FindByIdAsync(id);
            if (role == null) return NotFound();
            await _roleManager.DeleteAsync(role);

            return RedirectToAction("Roles");
        }


        [HttpGet]
        public async Task<IActionResult> EditRole(string id)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();
            var role = await _roleManager.FindByIdAsync(id);
            if (role == null) return NotFound();

            return View(role);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditRole(string id, string name)
        {
            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(name)) return NotFound();

            var role = await _roleManager.FindByIdAsync(id);
            if (role == null) return NotFound();
            role.Name = name;
            var res = await _roleManager.UpdateAsync(role);
            if (res.Succeeded) return RedirectToAction("Roles");

            foreach (var error in res.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            return View(role);
        }

        ////////////////////////////////{ User in Role  }//////////////////////////////////


        public IActionResult Users()
        {
            var model = _userManager.Users
                .Select(u => new IndexViewModel()
                { Id = u.Id, UserName = u.UserName, Email = u.Email }).ToList();
            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> EditUser(string id)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return NotFound();

            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditUser(string id, string name)
        {
            if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(name)) return NotFound();

            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return NotFound();
            user.UserName = name;
            var res = await _userManager.UpdateAsync(user);
            if (res.Succeeded) return RedirectToAction("Users");

            foreach (var error in res.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            return View(user);
        }

        [HttpGet]
        public async Task<IActionResult> AddUserToRole(string id)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return NotFound();
            var roles = _roleManager.Roles.ToList();
            var model = new AddUserToRoleViewModel() { UserId = id };

            foreach (var role in roles)
            {
                if (!await _userManager.IsInRoleAsync(user, role.Name))
                {
                    model.UserRolres.Add(new UserRoleViewModel()
                    {
                        RoleName = role.Name
                    });
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUserToRole(AddUserToRoleViewModel model)
        {
            if (model == null) return NotFound();

            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null) return NotFound();

            var reqRol = model.UserRolres.Where(r => r.IsSelected)
                .Select(u => u.RoleName).ToList();

            var res = await _userManager.AddToRolesAsync(user, reqRol);
            if (res.Succeeded) return RedirectToAction("Users");

            foreach (var error in res.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> RemoveUserToRole(string id)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return NotFound();
            var roles = _roleManager.Roles.ToList();
            var model = new AddUserToRoleViewModel() { UserId = id };

            foreach (var role in roles)
            {
                if (await _userManager.IsInRoleAsync(user, role.Name))
                {
                    model.UserRolres.Add(new UserRoleViewModel()
                    {
                        RoleName = role.Name
                    });
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveUserToRole(AddUserToRoleViewModel model)
        {
            if (model == null) return NotFound();

            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null) return NotFound();

            var reqRol = model.UserRolres.Where(r => r.IsSelected)
                .Select(u => u.RoleName).ToList();

            var res = await _userManager.RemoveFromRolesAsync(user, reqRol);
            if (res.Succeeded) return RedirectToAction("Users");

            foreach (var error in res.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteUser(string id)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return NotFound();
            await _userManager.DeleteAsync(user);

            return RedirectToAction("Users");

        }

        [HttpGet]
        public async Task<IActionResult> UpdateSecurityStamp(string id)
        {
            if (string.IsNullOrEmpty(id)) return NotFound();
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) return NotFound();
            await _userManager.UpdateSecurityStampAsync(user);
            return RedirectToAction("Users");
        }

        ////////////////////////////////{ Site settinge  }//////////////////////////////////


        public IActionResult settings()
        {
            var model = _dbContex.siteSettings.ToList();
            return View(model);
        }

        [HttpGet]
        public IActionResult RoleValidatioGuid()
        {
            var RoleValidatioGuidSite = _dbContex.siteSettings.FirstOrDefault(t => t.Key == "RoleValidationGuid");

            var model = new RoleValidatioGuidViewModel()
            {
                Value = RoleValidatioGuidSite?.Value,
                LastTimeChanged = RoleValidatioGuidSite?.LastTimeChanged
            };

            return View(model);
        }


        [HttpPost]
        public IActionResult RoleValidatioGuid(RoleValidatioGuidViewModel model)
        {
            var RoleValidatioGuidSite = _dbContex.siteSettings.FirstOrDefault(t => t.Key == "RoleValidationGuid");

            if (RoleValidatioGuidSite == null)
            {
                _dbContex.siteSettings.Add(new SiteSetting()
                {

                    Key = "RoleValidationGuid",
                    Value = Guid.NewGuid().ToString(),
                    LastTimeChanged = DateTime.Now
                });
            }
            else
            {
                RoleValidatioGuidSite.Value = Guid.NewGuid().ToString();
                RoleValidatioGuidSite.LastTimeChanged = DateTime.Now;
                _dbContex.Update(RoleValidatioGuidSite);
            }
            _dbContex.SaveChanges();
            _memoryCache.Remove("RoleValidationGuid");
            return RedirectToAction("settings");
        }

        ////////////////////////////////{ Site Role  }//////////////////////////////////


        public IActionResult SiteRole()
        {
            var roles = _roleManager.Roles.ToList();
            var model = new List<IndexViewModell>();
            foreach (var role in roles)
            {
                model.Add(new IndexViewModell()
                {
                    RoleName = role.Name,
                    RoleId = role.Id
                });
            }
            return View(model);
        }
        
        [HttpGet]
        public IActionResult CreateRole()
        {
            var allMvcNames =
                _memoryCache.GetOrCreate("AreaAndActionAndControllerNamesList", p =>
                {
                    p.AbsoluteExpiration = DateTimeOffset.MaxValue;
                    return _utilities.AreaAndActionAndControllerNamesList();
                });
            var model = new CreateRoleViewModel()
            {
                ActionAndControllerNames = allMvcNames
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleViewModel model) 
        {
            if (ModelState.IsValid)
            {
                var role = new IdentityRole(model.RoleName);
                var result = await _roleManager.CreateAsync(role);
                if (result.Succeeded)
                {
                    var requestRoles =
                        model.ActionAndControllerNames.Where(c => c.IsSelected).ToList();
                    foreach (var requestRole in requestRoles)
                    {
                        var areaName = (string.IsNullOrEmpty(requestRole.AreaName)) ?
                            "NoArea" : requestRole.AreaName;

                        await _roleManager.AddClaimAsync(role,
                            new Claim($"{areaName}|{requestRole.ControllerName}|{requestRole.ActionName}".ToUpper(),
                                true.ToString()));
                    }


                    return RedirectToAction("Index");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            return View(model);
        }
         
    }
}
